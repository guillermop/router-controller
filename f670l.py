import base64
import hashlib
import re
from typing import List

from bs4 import BeautifulSoup
from Crypto.Cipher import PKCS1_v1_5
from Crypto.PublicKey import RSA

from device import Device
from router import Router

PUB_KEY = """-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAodPTerkUVCYmv28SOfRV
7UKHVujx/HjCUTAWy9l0L5H0JV0LfDudTdMNPEKloZsNam3YrtEnq6jqMLJV4ASb
1d6axmIgJ636wyTUS99gj4BKs6bQSTUSE8h/QkUYv4gEIt3saMS0pZpd90y6+B/9
hZxZE/RKU8e+zgRqp1/762TB7vcjtjOwXRDEL0w71Jk9i8VUQ59MR1Uj5E8X3WIc
fYSK5RWBkMhfaTRM6ozS9Bqhi40xlSOb3GBxCmliCifOJNLoO9kFoWgAIw5hkSIb
GH+4Csop9Uy8VvmmB+B3ubFLN35qIa5OG5+SDXn4L7FeAA5lRiGxRi8tsWrtew8w
nwIDAQAB
-----END PUBLIC KEY-----"""


class F670L(Router):
    def __init__(self, host: str, username: str, password: str) -> None:
        super().__init__(host, username, password)
        self._headers = {
            "Content-Type": "application/x-www-form-urlencoded",
            "Accept": "application/json, text/javascript",
            "Accept-Encoding": "gzip, deflate",
            "Referer": self._host,
            "Connection": "keep-alive",
            "Cookie": "_TESTCOOKIESUPPORT=1",
        }
        self._sid = None
        self._session_token = None

    def login(self):
        token = self._get_login_token()
        xml_obj = self._get_xml_obj(token)

        hashed = hashlib.sha256(
            f"{self._password}{xml_obj}".encode("utf-8")
        ).hexdigest()

        res = self._session.post(
            f"{self._host}?_type=loginData&_tag=login_entry",
            headers=self._headers,
            data={
                "action": "login",
                "Password": hashed,
                "Username": self._username,
                "_sessionTOKEN": token,
            },
        )

        self._session_token = res.json()["sess_token"]
        self._headers["Cookie"] += f"; SID={res.cookies.get_dict()['SID']}"

        if res.status_code == 200:
            json = res.json()
            if "login_need_refresh" in json:
                self._session_token = json["sess_token"]
                self._headers["Cookie"] += f"; SID={res.cookies.get_dict()['SID']}"
                self._connected = True

        return self._connected

    @Router.authorize
    def logout(self):
        self._session.post(
            f"{self._host}?_type=loginData&_tag=logout_entry",
            headers=self._headers,
            data={
                "IF_LogOff": "1",
            },
        )
        self._connected = False

    @Router.authorize
    def reboot(self):
        tmp_token = self._get_session_tmp_token("rebootAndReset")

        data = f"IF_ACTION=Restart&Btn_restart=&_sessionTOKEN={tmp_token}"

        headers = {**self._headers, "Check": self._asymmetric_encrypt(data)}

        self._session.post(
            f"{self._host}?_type=menuData&_tag=devmgr_restartmgr_lua.lua",
            headers=headers,
            data=data,
        )

    def change_password(self, new_password, new_username):
        raise NotImplementedError("change password not supported")

    @Router.authorize
    def set_wifi_enabled(self, status):
        tmp_token = self._get_session_tmp_token("wlanBasic")

        status = int(status)

        res = self._session.get(
            f"{self._host}?_type=menuData&_tag=wlan_wlanbasiconoff_lua.lua"
        )

        soup = BeautifulSoup(res.text, "xml")
        values = [p.text for p in soup.find_all("ParaValue")]

        data = (
            "IF_ACTION=Apply"
            f"&_InstID={values[0]}"
            f"&TimerEnable={values[1]}"
            f"&_InstID_0={values[7]}"
            f"&_InstID_1={values[10]}"
            f"&RadioStatus_0={status}"
            f"&RadioStatus_1={status}"
            f"&_sessionTOKEN={tmp_token}"
        )

        headers = {**self._headers, "Check": self._asymmetric_encrypt(data)}

        res = self._session.post(
            f"{self._host}?_type=menuData&_tag=wlan_wlanbasiconoff_lua.lua",
            headers=headers,
            data=data,
        )

    def set_access_control_enabled(self, status: bool):
        raise NotImplementedError("set access control devices not supported")

    def get_access_control_devices(self) -> List[Device]:
        raise NotImplementedError("get access control devices not supported")

    def update_access_control_devices(self, devices: List[Device]):
        raise NotImplementedError("update access control devices not supported")

    def _asymmetric_encrypt(self, text):
        hashed = hashlib.sha256(text.encode("utf-8")).hexdigest()

        rsa_key = RSA.import_key(PUB_KEY)
        cipher = PKCS1_v1_5.new(rsa_key)
        encrypted = cipher.encrypt(hashed.encode("utf-8"))

        return base64.b64encode(encrypted)

    def _get_session_tmp_token(self, tag):
        pattern = re.compile('_sessionTmpToken = "(.*?)";')

        res = self._session.get(
            f"{self._host}?_type=menuView&_tag={tag}&Menu3Location=0",
            headers=self._headers,
        )

        soup = BeautifulSoup(res.text, "html.parser")

        if script := soup.find("script", string=pattern):
            if match := pattern.search(script.text):
                return bytes.fromhex(match.group(1).replace("\\x", "")).decode("utf-8")

    def _get_login_token(self):
        res = self._session.get(f"{self._host}?_type=loginData&_tag=login_entry").json()
        return res["sess_token"]

    def _get_xml_obj(self, token):
        res = self._session.get(
            f"{self._host}?_type=loginData&_tag=login_token&_={token}"
        )
        soup = BeautifulSoup(res.text, "xml")
        if root := soup.find("ajax_response_xml_root"):
            return root.text
