from dataclasses import dataclass


@dataclass
class Device:
    name: str
    mac: str
