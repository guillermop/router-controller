from typing import List
from urllib.parse import quote, unquote

from device import Device
from router import Router

SALT = "RDpbLfCPsJZ7fiv"
DIC = (
    "yLwVl0zKqws7LgKPRQ84Mdt708T1qQ3Ha7xv3H7NyU84p21BriUWBU43odz3iP4r"
    "BL3cD02KZciXTysVXiV8ngg6vL48rPJyAUw0HurW20xqxv9aYb4M9wK1Ae0wlro5"
    "10qXeU07kV57fQMc8L6aLgMLwygtc0F10a0Dg70TOoouyFhdysuRMO51yY5ZlOZZ"
    "LEal1h0t9YQW0Ko7oBwmCAHoic4HYbUyVeU3sfQ1xtXcPcf1aT303wAQhv66qzW"
)


class TLWA855RE(Router):
    def __init__(self, host: str, password: str) -> None:
        super().__init__(host, "", password)
        self._headers = {
            "Content-Type": "text/html;charset=UTF-8",
            "Accept": "text/plain, */*; q=0.01",
            "Accept-Encoding": "gzip, deflate",
            "Referer": host,
            "Connection": "keep-alive",
        }
        self._auth_info = self._get_auth_info()

    def _get_auth_info(self):
        res = self._session.post(
            f"{self._host}?code=7&asyn=1",
            headers=self._headers,
        )
        parts = res.text.split("\r")
        return parts[3].strip(), parts[4].strip()

    def _encrypt(self, text, salt=SALT, d=DIC):
        length = max(len(text), len(salt))
        text = text.ljust(length, chr(187))
        salt = salt.ljust(length, chr(187))
        return "".join(
            [d[(ord(text[i]) ^ ord(salt[i])) % len(d)] for i in range(length)]
        )

    def login(self):
        encrypted = self._encrypt(self._password)
        self._id = self._encrypt(self._auth_info[0], encrypted, self._auth_info[1])
        res = self._session.post(
            f"{self._host}?code=7&asyn=0&id={self._id}",
            headers=self._headers,
        )

        self._connected = res.status_code == 200

        return self._connected

    @Router.authorize
    def logout(self):
        self._session.post(
            f"{self._host}?code=11&asyn=0&id={self._id}",
            headers=self._headers,
        )
        self._connected = False

    @Router.authorize
    def reboot(self):
        self._session.post(
            f"{self._host}?code=6&asyn=1&id={self._id}",
            headers=self._headers,
        )

    @Router.authorize
    def change_password(self, new_password, new_username=None):
        new_password = self._encrypt(new_password)
        current_password = self._encrypt(self._password)
        self._session.post(
            f"{self._host}?code=10&asyn=0&auth={current_password}&id={self._id}",
            headers=self._headers,
            data=new_password,
        )

    def set_wifi_enabled(self, status):
        raise NotImplementedError("set wifi enable not supported")

    @Router.authorize
    def set_access_control_enabled(self, status: bool):
        self._session.post(
            f"{self._host}?code=1&asyn=0&id={self._id}",
            headers=self._headers,
            data=f"id 31|1,0,0\rbhavEnable {int(status)}\rbhavRule 1",
        )

    @Router.authorize
    def get_access_control_devices(self):
        res = self._session.post(
            f"{self._host}?code=2&asyn=0&id={self._id}",
            headers=self._headers,
            data="60|1,0,0",
        )
        rows = res.text.split("\r")[2:-18]
        devices = []
        for i in range(16):
            name = rows[i].split(" ")[-1]
            if not name:
                break
            mac = rows[i - 16].split(" ")[-1]
            devices.append(Device(unquote(name), mac.replace("-", ":").upper()))
        return devices

    @Router.authorize
    def update_access_control_devices(self, devices: List[Device]):
        saved = self.get_access_control_devices()

        for dev in [dev for dev in saved if dev not in devices]:
            index = saved.index(dev)
            del saved[index]
            self._ac_remove_device(index)

        for dev in [dev for dev in devices if dev not in saved]:
            self._ac_add_device(dev)

    def _ac_add_device(self, device: Device):
        mac = device.mac.replace(":", "-").upper()
        name = quote(device.name)
        command = f"advanced bm host -add id:60 name:{name} mac:{mac} devType:other"
        self._session.post(
            f"{self._host}?code=0&asyn=0&id={self._id}",
            headers=self._headers,
            data=command,
        )

    def _ac_remove_device(self, index):
        command = f"advanced bm host -delete id:60 index:{index}"
        self._session.post(
            f"{self._host}?code=0&asyn=0&id={self._id}",
            headers=self._headers,
            data=command,
        )
