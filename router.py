from abc import ABC, abstractmethod
from functools import wraps
from typing import List, Self

import requests

from device import Device


class Router(ABC):
    def __init__(self, host: str, username: str, password: str) -> None:
        self._host = host
        self._password = password
        self._username = username
        self._connected = False
        self._session = requests.session()

    @property
    def connected(self):
        return self._connected

    @abstractmethod
    def login(self) -> bool:
        pass

    @abstractmethod
    def logout(self):
        pass

    @abstractmethod
    def reboot(self):
        pass

    @abstractmethod
    def change_password(self, new_password: str, new_username: str):
        pass

    @abstractmethod
    def set_wifi_enabled(self, status: bool):
        pass

    @abstractmethod
    def set_access_control_enabled(self, status: bool):
        pass

    @abstractmethod
    def get_access_control_devices(self) -> List[Device]:
        pass

    @abstractmethod
    def update_access_control_devices(self, devices: List[Device]):
        pass

    @staticmethod
    def authorize(f):
        @wraps(f)
        def decorated(s: Self, *args):
            if s.connected:
                return f(s, *args)
            else:
                raise Exception("Not connected")

        return decorated
