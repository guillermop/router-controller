import csv
from os import environ

from dotenv import load_dotenv

from device import Device
from tl_wa855re import TLWA855RE

load_dotenv(override=True)


def read_devices_file():
    devices = []
    with open("devices.csv", newline="", mode="r") as csvfile:
        reader = csv.reader(csvfile, delimiter=",", quotechar="|")
        _header = next(reader)
        for row in reader:
            devices.append(Device(row[0], row[1]))
    return devices


def sync_tlwa855re(devices):
    host = environ.get("HOST")
    password = environ.get("PASSWORD")

    if not (host and password):
        return

    router = TLWA855RE(host, password)
    try:
        success = router.login()
        if not success:
            return
        router.update_access_control_devices(devices)
    finally:
        router.logout()


def main():
    devices = read_devices_file()
    sync_tlwa855re(devices)


if __name__ == "__main__":
    main()
