from typing import List

from bs4 import BeautifulSoup

from device import Device
from router import Router


class DPC3940ADL(Router):
    def __init__(self, host: str, username: str, password: str) -> None:
        super().__init__(host, username, password)
        self._headers = {
            "Content-Type": "application/x-www-form-urlencoded",
            "Accept": "text/html",
            "Accept-Encoding": "gzip, deflate",
            "Referer": host,
            "Connection": "keep-alive",
        }
        self._default_lang = "en"

    def login(self):
        token = self._get_web_token("logon.asp")
        res = self._session.post(
            f"{self._host}goform/logon",
            headers=self._headers,
            data={
                "Username": self._username,
                "Password": self._password,
                "LanguageSelect": self._default_lang,
                "webToken": token,
            },
        )
        soup = BeautifulSoup(res.text, "html.parser")
        self._connected = (
            soup.find("title", attrs={"data-i18n": "login_header"}) is None
        )
        return self._connected

    @Router.authorize
    def logout(self):
        self._session.get(f"{self._host}log_out.asp")
        self._connected = False

    @Router.authorize
    def reboot(self):
        token = self._get_web_token("ad_restart_gateway.asp")
        self._session.post(
            f"{self._host}goform/ad_restart_gateway",
            headers=self._headers,
            data={
                "devicerestart": "1",
                "webToken": token,
            },
        )

    @Router.authorize
    def change_password(self, new_password, new_username):
        token = self._get_web_token("ad_change_password.asp")
        self._session.post(
            f"{self._host}goform/ad_change_password",
            headers=self._headers,
            data={
                "new_username": new_username,
                "new_password": new_password,
                "new_password_confirm": new_password,
                "h_user_type": "common/",
                "webToken": token,
            },
        )

    @Router.authorize
    def set_wifi_enabled(self, status: bool):
        token = self._get_web_token("wi_radio_settings.asp")
        self._session.post(
            f"{self._host}goform/wi_radio_settings",
            headers=self._headers,
            data={
                "wi_24_enable": int(status),
                "wi_5_enable": int(status),
                "webToken": token,
            },
        )

    @Router.authorize
    def update_access_control(self, devices, enable=True, allow=True):
        token = self._get_web_token("wi_access_control.asp")
        enabled = int(enable)
        allow = 1 if allow else 2

        payload = {
            "webToken": token,
            "accessControlEnable": enabled,
            "accessModeAllow": allow,
        }
        for i, dev in enumerate(devices):
            payload[f"wl_device{i}"] = dev[0]
            payload[f"wl_mac{i}"] = dev[1]
        for i in range(len(devices), 32):
            payload[f"wl_device{i}"] = "----"
            payload[f"wl_mac{i}"] = "00:00:00:00:00:00"

        for i in range(2):
            payload["wl_CurrentNetworks"] = i
            self._session.post(
                f"{self._host}goform/wi_access_control",
                headers=self._headers,
                data=payload,
            )
            self._session.post(
                f"{self._host}goform/wi_access_control",
                headers=self._headers,
                data=payload,
            )

    @Router.authorize
    def set_access_control_enabled(self, status: bool):
        token = self._get_web_token("wi_access_control.asp")

        payload = {
            "webToken": token,
            "accessControlEnable": int(status),
            "accessModeAllow": 1,
        }

        for i in range(2):
            payload["wl_CurrentNetworks"] = i
            self._session.post(
                f"{self._host}goform/wi_access_control",
                headers=self._headers,
                data=payload,
            )
            self._session.post(
                f"{self._host}goform/wi_access_control",
                headers=self._headers,
                data=payload,
            )

    @Router.authorize
    def get_access_control_devices(self) -> List[Device]:
        res = self._session.get(f"{self._host}wi_access_control.asp")
        soup = BeautifulSoup(res.text, "html.parser")
        table = soup.find("table", attrs={"id": "deviceTable"})
        devices = []
        for tr in table.find_all("tr")[1:]:  # type: ignore
            mac, name = tr.find_all("td")[0:2]
            devices.append(Device(name.text.strip(), mac.text.strip()))
        return devices

    @Router.authorize
    def update_access_control_devices(self, devices: List[Device]):
        token = self._get_web_token("wi_access_control.asp")
        payload = {
            "webToken": token,
        }
        for i, dev in enumerate(devices):
            payload[f"wl_device{i}"] = dev.name
            payload[f"wl_mac{i}"] = dev.mac
        for i in range(len(devices), 32):
            payload[f"wl_device{i}"] = "----"
            payload[f"wl_mac{i}"] = "00:00:00:00:00:00"

        for i in range(2):
            payload["wl_CurrentNetworks"] = i
            self._session.post(
                f"{self._host}goform/wi_access_control",
                headers=self._headers,
                data=payload,
            )
            self._session.post(
                f"{self._host}goform/wi_access_control",
                headers=self._headers,
                data=payload,
            )

    def _get_web_token(self, route):
        res = self._session.get(f"{self._host}{route}")
        soup = BeautifulSoup(res.text, "html.parser")
        hidden = soup.find("input", attrs={"type": "hidden", "name": "webToken"})
        return hidden.get("value")  # type: ignore
